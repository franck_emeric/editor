import { Component, Input, OnInit } from '@angular/core';
import { ColorEvent } from 'ngx-color';
import Typed from 'typed.js';
import { fabric } from 'fabric';
import { EditorserviceService } from 'src/app/core';
import { HttpservicesService } from 'src/app/core';
import { EditService } from 'src/app/core/edit.service';
declare var require :any
var $ = require("jquery")
@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {
@Input() data:any;
current_page:any=1
width=400;
height=400
canvas:any;
iH=400;
iW=400
hastext=false;
canvaswrapper:any
text:any;
testingpicture:any
textalign=["left","center","justify","right"]
cpt=0;
state:any
price:any
promoprice:any
details:any
models: any
articlename:any
num_category:any
clipart:any
undo:any=[];
redo:any=[];
qty:any
faces:any=[]
productstate:any=[]
isdone=false
Isokey=false
category=['Vêtement','Affichage','Emballage','Imprimerie','Gadget']


 	//id_model 	front 	back 	obj 	price 	category 	promo 	qty 	type 	added_at 	updated_at 	owner 	description
testcanvas:any;
colors=["blue","white","black","red","orange","yellow","green","#999999","#454545","#800080","#000080","#00FF00","#800000","#008080"]
  constructor(private Editor:EditorserviceService,private http:HttpservicesService, private Edit:EditService) { }

  ngOnInit(): void {

    //let typed= new Typed ('#typed',{
    //  strings:['Inscrivez-vous et continuez votre achat.'],
    // // typeSpeed:140,
     // showCursor:false,
     // 
    //  
    //});
console.log(JSON.parse(this.data.description))
    this.http.getclipart(this.current_page).subscribe(res=>{
      this.clipart=res;
  
      console.log(this.clipart.cliparts)
    this.clipart=this.clipart.cliparts;
  },
  er=>{console.log(er)})

    this.testcanvas= new fabric.Canvas('test',{
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor:'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful:true,
      stopContextMenu:false

    });


  this.canvas= new fabric.Canvas('canvas-wrapper',{
    hoverCursor: 'pointer',
    selection: true,
    selectionBorderColor:'blue',
    fireRightClick: true,
    preserveObjectStacking: true,
    stateful:true,
    stopContextMenu:false

  });
  this.canvas.filterBackend=new fabric.WebglFilterBackend();
  var url=JSON.parse(this.data.description).url
  var url2=JSON.parse(this.data.description).url2
  this.faces.push(url,url2)
   this.Editor.MakeImage(url, this.canvas)
var canvasWrapper:any = document.getElementById('wrapper');
// initial dimensions
canvasWrapper.style.width = this.iW;
canvasWrapper.style.height = this.iH;

setInterval(() => {
  const newWidth = canvasWrapper.clientWidth
  const newHeight = canvasWrapper.clientHeight
  if (newWidth !== this.iW || newHeight !== this.iH) {
    this.width = newWidth
    this.height = newHeight
    this.iW=this.width;
    this.iH=this.height
    this.canvas.setWidth(newWidth)
    this.canvas.setHeight(newHeight)
  }
},100)

  this.canvas.setWidth(this.width);
  this.canvas.setHeight(this.height);
  this.Editor.MakeImage(this.data.url,this.canvas);


}


  makeItalic(){
    this.Editor.italic(this.canvas)

  }

  makeBold(){
    this.Editor.bold(this.canvas)
  }

  underlineText(){
    this.Editor.underline(this.canvas)
  }


  overlineText(){
    this.Editor.overline(this.canvas)
  }


  addText(){
    if(!this.hastext){
      this.Editor.addText(this.canvas);

      this.hastext=true;
    }
  }


  copy(){
    this.Editor.copy(this.canvas)
  }

  paste(){
    this.Editor.paste(this.canvas)
  }


  removeItem(){

    this.Editor.remove(this.canvas);
  }

  InputChange(Inputtext:any){
    if(this.canvas.getActiveObject()!=undefined && this.canvas.getActiveObject().text){

      if(this.cpt==0){
        this.text=this.canvas.getActiveObject().text+" "+ this.text
        this.cpt=this.cpt+1
        this.canvas.getActiveObject().text= this.text
        this.canvas.requestRenderAll();
      }else{
        this.canvas.getActiveObject().text= this.text
        this.canvas.requestRenderAll();
      }

    }else{

      let text= new fabric.Textbox(this.text,{
        top:200,
        left:200,
        fill:"blue",
        fontSize:38,
        fontStyle:'normal',
        cornerStyle:'circle',
        selectable:true,
        borderScaleFactor:1,
        overline:false,
        lineHeight:1.5
      });

      this.canvas.add(text).setActiveObject(text);
      this.canvas.renderAll(text);
      this.canvas.requestRenderAll();
      this.canvas.centerObject(text);
    }


  }

  texteclor($event:ColorEvent){
    this.Editor.textcolor($event.color.hex,this.canvas);

  }



  setItem(event:any){
  this.Editor.setitem(event,this.canvas)
  }


  onFileUpload(event:any){
    let file = event.target.files[0];
    if(!this.Editor.handleChanges(file)){

      const reader = new FileReader();

    reader.onload = () => {
      let url:any = reader.result;
      fabric.Image.fromURL(url,(oImg) =>{
      oImg.set({
          scaleX:0.5,
          scaleY:0.5,
          crossOrigin: "Anonymous",
    });
      this.canvas.add(oImg).setActiveObject(oImg);
      this.canvas.centerObject(oImg);
      this.canvas.renderAll(oImg)

    })
      };
      reader.readAsDataURL(file);
    console.log(file)
    }

  }


  resize(){
    this.canvas.setWidth(this.width);
    this.canvas.setHeight(this.height);
    this.canvas.centeredScaling=true
     this.canvas.renderAll()

  }


  InputSize(){
    var canvasWrapper:any = document.getElementById('wrapper');
    canvasWrapper.style.width = this.width;
canvasWrapper.style.height = this.height;
this.canvas.setWidth(this.width);
this.canvas.setHeight(this.width);
  }
// initial dimensions


  getModel(){
    this.models={

      description:JSON.stringify({
        name:this.articlename,
        made_with:this.details,
        price:this.price,
        promo:this.promoprice,
        owner:76,
        qty:this.qty,
        category:this.num_category,
        type:"model",
    
      })

    }
    console.log(this.models)
    
    for(let item of this.canvas.getObjects()){
     item.set({lockMovementX:false})
     item.set({lockMovementY:false})
     item.set({lockScalingY:true})
     item.set({lockScalingX:true})
    }
    var json = this.canvas.toJSON(['lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY'])
   // this.testcanvas.loadFromJSON(json, this.testcanvas.renderAll.bind(this.testcanvas));
    //this.testcanvas.setHeight(this.height);
   // this.testcanvas.setWidth(this.width);
    
    Object.assign(this.models,{obj:JSON.stringify(json),width:this.width,height:this.height})

    this.http.add(this.models).subscribe(
      res=>{
        console.log(res)
      },
      err=>{
        console.log(err)
      }
    )
    
  }
  changeCategories(event:any){
    if(event.target.value == "Affichage"){
      this.num_category = 3
    }
    if(event.target.value == "Gadget"){
      this.num_category = 5
    }
    if(event.target.value == "Emballage"){
      this.num_category = 2
    }
    if(event.target.value == "Vêtement"){
      this.num_category = 1
    }
    if(event.target.value == "Imprimerie"){
      this.num_category = 4
    }
    console.log(this.num_category)
  }
  currentPage(event:any){
    var page=event.target.id
      if(+page){
        this.current_page=page
        
        this.http.getclipart(page).subscribe(res=>{
          let data:any=res;
          if(data.status==200){
            if(data.cliparts.length>0){
              
              this.clipart=data.cliparts
            }else{
             
            }
          
          }else{
           
          }
        }, )
      }
  
  }
  sendBack(){
    this.Edit.sendBack(this.canvas)
  }
  
  sendForward(){
    this.Edit.sendForward(this.canvas)
  }
  textAlign(val:any){
    this.Edit.textAlign(this.canvas,val)
  
  }
  replay(playStack:any, saveStack:any, buttonsOn:any, buttonsOff:any) {
    saveStack.push(this.state);
    this.state = playStack.pop();
    var on = $(buttonsOn);
    var off = $(buttonsOff);
    // turn both buttons off for the moment to prevent rapid clicking
    on.prop('disabled', true);
    off.prop('disabled', true);
    this.canvas.clear();
    this.canvas.loadFromJSON(this.state,()=> {
      this.canvas.renderAll();
      // now turn the buttons back on if applicable
      on.prop('disabled', false);
      if (playStack.length) {
        off.prop('disabled', false);
      }
    });
  }
  Redo(){
    $('#redo').click((e:any)=> {
      this.replay(this.redo, this.undo, '#undo',e);
    })
  }
  Undo(){
    $('#undo').click((e:any)=> {
      this.replay(this.undo,this.redo, '#redo', e);
    });
   
  }

register(){
  
  
  if(this.isdone==false){
  var json = this.canvas.toJSON(['lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY'])
  this.productstate.push(JSON.stringify(json))
  this.faces[0]=this.canvas.toDataURL()

  console.log(this.faces[1])
  if(this.faces[1]!=undefined){
    this.canvas.clear()
    this.canvas.renderAll()
    this.Editor.MakeImage(this.faces[1],this.canvas)

    this.isdone=true
  }else{
    this.isdone=true
  }
  
  }else{
    var json2= this.canvas.toJSON(['lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY'])
    this.productstate.push(JSON.stringify(json2))
    this.faces[1]=this.canvas.toDataURL()
   var data:any={}
   var gil=JSON.parse(this.data.description)
   Object.assign(data, {
    description:JSON.stringify({

      name:gil.name,
      made_with:gil.made_with,
      price:gil.price,
      promo:gil.promo,
      owner:gil.owner,
      qty:gil.qty,
      size:gil.size,
      material:gil.material,
      category:gil.category,
      type:gil.type, 
      
    }),
    obj:this.productstate[0],
    objf:this.productstate[1],
    width:this.width,
    height:this.height

   })
   
     this.Spinner()
   
   this.http.add(data).subscribe(
      res=>{
        console.log(res)
        if(res.insertId){
          this.Spinner()
          alert("bien")
        }
      },
      err=>{
        console.log(err)
        this.Spinner()
      }
    )
  }
}
Spinner(){
  this.Isokey=!this.Isokey
}
}
