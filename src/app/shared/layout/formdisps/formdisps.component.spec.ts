import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormdispsComponent } from './formdisps.component';

describe('FormdispsComponent', () => {
  let component: FormdispsComponent;
  let fixture: ComponentFixture<FormdispsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormdispsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormdispsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
